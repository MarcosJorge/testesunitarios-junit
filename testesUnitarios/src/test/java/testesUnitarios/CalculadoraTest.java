/**
 * 
 */
package testesUnitarios;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Marcos Jorge
 *
 */
public class CalculadoraTest {
	
	@Test
	public void teste() {
		// TODO Auto-generated method stub
		Calculadora calcular = new Calculadora();
		
		String a = "3";
		String b = "5";
		
		Assert.assertEquals(8, calcular.somar(a, b));
		
		Assert.assertEquals(15, calcular.multiplicar(a, b));
		
	}
}
