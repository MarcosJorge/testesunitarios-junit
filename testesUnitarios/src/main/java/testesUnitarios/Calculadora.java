/**
 * 
 */
package testesUnitarios;

/**
 * @author Marcos Jorge
 *
 */
public class Calculadora implements InterfaceCalculadora {

	public int somar(String digito1, String digito2) {
		int soma = Integer.parseInt(digito1 + digito2); // TODO Auto-generated method stub
		return soma;
	}

	public double dividir(String dividendo, String divisor) {
		double divisao = Double.parseDouble(dividendo)/Double.parseDouble(divisor);
		return divisao;
	}

	public double raiz(String numero) {
		// TODO Auto-generated method stub
		return 0;
	}

	public double multiplicar(String numero1, String numero2) {
		// TODO Auto-generated method stub
		int resultado = Integer.parseInt(numero1)*Integer.parseInt(numero2);
		return resultado;
	}

}
