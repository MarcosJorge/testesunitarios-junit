/**
 * 
 */
package testesUnitarios;

/**
 * @author Marcos Jorge
 *
 */
public interface InterfaceCalculadora {

	public int somar(String digito1,String digito2);
	
	public double dividir(String dividendo, String divisor);
	
	public double raiz(String numero);
	
	public double multiplicar(String numero1,String numero2);
	
}
